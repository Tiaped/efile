package org.egedede.efile.viewer.imageio;

import org.egedede.efile.viewer.ViewerContext;
import org.egedede.efile.viewer.ViewerResult;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

public class TestImageViewer {

  @Test
  public void testMe() throws IOException {
    ImageViewer viewer = new ImageViewer();
    InputStream stream = getResourceAsStream("example.jpg");
    ViewerResult process = viewer.process(new ViewerContext(stream));
    Assertions.assertTrue(process.getStatus().isSuccess(), "Preview should have been processed successfully");
    Assertions.assertArrayEquals(
        getResourceAsStream("example.png").readAllBytes(),
        process.getResult().readAllBytes(),
        "Image Preview does not match expected"
    );
  }

  private InputStream getResourceAsStream(String path) {
    return getClass().getClassLoader().getResourceAsStream(path);
  }
}
