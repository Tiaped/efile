package org.egedede.efile.viewer.imageio;

import org.egedede.efile.viewer.Viewer;
import org.egedede.efile.viewer.ViewerContext;
import org.egedede.efile.viewer.ViewerResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Iterator;

public class ImageViewer implements Viewer {
  private static final Logger LOGGER = LoggerFactory.getLogger(ImageViewer.class);

  @Override
  public ViewerResult process(ViewerContext context) {
    try (ImageInputStream imageInputStream = ImageIO.createImageInputStream(context.getSource())) {
      Iterator<ImageReader> imageReaders = ImageIO.getImageReaders(imageInputStream);
      if (!imageReaders.hasNext()) {
        return ViewerResult.of(context, "No image reader found");
      }
      ImageReader reader = imageReaders.next();
      reader.setInput(imageInputStream);
      try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
        ImageIO.write(reader.read(0), "png", outputStream);
        return ViewerResult.of(context, new ByteArrayInputStream(outputStream.toByteArray()));
      } finally {
        reader.dispose();
      }
    } catch (Throwable throwable) {
      return ViewerResult.of(context, throwable);
    }
  }
}
