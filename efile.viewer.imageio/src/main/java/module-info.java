module efile.viewer.imageio {
  requires java.desktop;
  requires efile.viewer;
  requires org.slf4j;
}