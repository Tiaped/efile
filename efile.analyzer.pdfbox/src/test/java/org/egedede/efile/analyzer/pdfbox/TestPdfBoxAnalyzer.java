package org.egedede.efile.analyzer.pdfbox;

import org.egedede.efile.analyzer.AnalyzeResult;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.net.URISyntaxException;

public class TestPdfBoxAnalyzer {

  @Test
  public void testPdfAnalyzer() throws URISyntaxException {
    URI uri = getClass().getClassLoader().getResource("Grille_tarifaire_Bayard_Milan.pdf").toURI();
    PdfBoxAnalyzer analyzer = new PdfBoxAnalyzer();
    AnalyzeResult analyze = analyzer.analyze(uri);
    System.out.println(analyze.getResult());
  }
}
