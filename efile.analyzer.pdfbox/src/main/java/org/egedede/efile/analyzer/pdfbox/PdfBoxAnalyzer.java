package org.egedede.efile.analyzer.pdfbox;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.form.PDFormXObject;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.egedede.efile.analyzer.AnalyzeResult;
import org.egedede.efile.analyzer.AnalyzeStatus;
import org.egedede.efile.analyzer.Analyzer;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;

public class PdfBoxAnalyzer implements Analyzer {
  @Override
  public AnalyzeResult analyze(URI uri) {
    AnalyzeResult result = new AnalyzeResult();
    result.newResult("PdfBox:");
    try {
      PDDocument document = PDDocument.load(new File(uri));
      result.newResult("\tEncryption: %s", document.getEncryption());
      result.newResult("\tAuthor: %s", document.getDocumentInformation().getAuthor());
      result.newResult("\tCreator: %s", document.getDocumentInformation().getCreator());
      result.newResult("\tCreation date: %s", document.getDocumentInformation().getCreationDate());
      result.newResult("\tProducer: %s", document.getDocumentInformation().getProducer());
      result.newResult("\tSubject: %s", document.getDocumentInformation().getSubject());
      result.newResult("\tTitle: %s", document.getDocumentInformation().getTitle());
      result.newResult("\tModification date : %s", document.getDocumentInformation().getModificationDate());
      result.newResult("\tKeywords: %s", document.getDocumentInformation().getKeywords());
      result.newResult("\tTrapped: %s", document.getDocumentInformation().getTrapped());
      result.newResult("\tMetadata: ");
      for (String metadata : document.getDocumentInformation().getMetadataKeys()) {
        result.newResult("\t * %s : %s", metadata, document.getDocumentInformation().getCustomMetadataValue(metadata));
      }
      result.newResult("\tnb pages: %s", document.getNumberOfPages());
      for (int i = 0; i < document.getNumberOfPages(); i++) {
        result.newResult("\t Page %s", i + 1);
        PDPage page = document.getPage(i);
        PDResources pdResources = page.getResources();
        int imageCount = 1;
        for (COSName c : pdResources.getXObjectNames()) {
          PDXObject o = pdResources.getXObject(c);
          if (o instanceof org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject) {
            PDImageXObject image = (PDImageXObject) o;
            result.newResult("\t\tImage %s", imageCount++);
            BufferedImage javaImage = image.getImage();
            result.newResult("\t\t\tWidth x Height : %s x %s", javaImage.getWidth(), javaImage.getHeight());
            result.newResult("\t\t\tNb Components %s", javaImage.getColorModel().getComponentSize().length);
            result.newResult("\t\t\t");
            result.newResult("\t\t\t");
          } else if(o instanceof PDFormXObject){
            PDFormXObject form = (PDFormXObject) o;
            result.newResult("\t\tForm %s", imageCount++);
          } else {
            result.newResult("\t\t not managed resource %s", o.getClass());
          }
        }
      }


      result.setStatus(AnalyzeStatus.SUCCESS);
    } catch (IOException e) {
      result.setStatus(AnalyzeStatus.ERROR);
      result.newResult("Failed to analyze %s : %s", uri, e.getMessage());
    }
    return result;
  }
}
