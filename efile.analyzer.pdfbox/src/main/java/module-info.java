module efile.viewer.imageio {
  requires java.desktop;
  requires efile.analyzer;
  requires org.slf4j;
  requires org.apache.pdfbox;
}