package org.egedede.efile.viewer;

import java.io.InputStream;

public class ViewerContext {

  private final InputStream source;

  public ViewerContext(InputStream source) {
    this.source = source;
  }

  public InputStream getSource() {
    return source;
  }
}
