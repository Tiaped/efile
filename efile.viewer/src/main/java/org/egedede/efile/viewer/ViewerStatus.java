package org.egedede.efile.viewer;

public interface ViewerStatus {
  boolean isSuccess();

  default boolean isFailure() {
    return ! isSuccess();
  }
}
