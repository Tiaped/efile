package org.egedede.efile.viewer;

/**
 * Kind of either.
 * Either an exception or a functional issue provided as a message.
 * => shall everything be handled as an Exception (wrong page, time out)
 */
public class ErrorCause {

  private final Throwable throwable;
  private final String message;

  private ErrorCause(Throwable throwable, String message) {
    this.throwable = throwable;
    this.message = message;
  }

  public static ErrorCause of(String errorMessage) {
    return new ErrorCause(null, errorMessage);
  }
  public static ErrorCause of(Throwable throwable) {
    return new ErrorCause(throwable, null);
  }

  public Throwable getThrowable() {
    return throwable;
  }

  public String getMessage() {
    return message;
  }
}
