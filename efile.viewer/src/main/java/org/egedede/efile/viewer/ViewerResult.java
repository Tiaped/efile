package org.egedede.efile.viewer;

import java.io.InputStream;

public class ViewerResult {

  private final ViewerStatus status;
  private final ViewerContext context;
  private final ErrorCause error;
  private final InputStream result;

  private ViewerResult(ViewerStatus status, ViewerContext context, ErrorCause error, InputStream result) {
    this.status = status;
    this.context = context;
    this.error = error;
    this.result = result;
  }

  public static ViewerResult of(ViewerContext context, InputStream result) {
    return new ViewerResult(
        new ViewerStatus() {
          @Override
          public boolean isSuccess() {
            return true;
          }
        }, context, null, result
    );
  }
  public static ViewerResult of(ViewerContext context, String errorMessage) {
    return new ViewerResult(
        new ViewerStatus() {
          @Override
          public boolean isSuccess() {
            return false;
          }
        }, context, ErrorCause.of(errorMessage), null
    );
  }
  public static ViewerResult of(ViewerContext context, Throwable throwable) {
    return new ViewerResult(
        new ViewerStatus() {
          @Override
          public boolean isSuccess() {
            return false;
          }
        }, context, ErrorCause.of(throwable), null
    );
  }
  public ViewerStatus getStatus() {
    return status;
  }

  public ViewerContext getContext() {
    return context;
  }

  public ErrorCause getError() {
    return error;
  }

  public InputStream getResult() {
    return result;
  }
}
