package org.egedede.efile.viewer;

public interface Viewer {

  ViewerResult process(ViewerContext context);
}
