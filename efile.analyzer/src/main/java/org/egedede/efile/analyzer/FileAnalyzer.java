package org.egedede.efile.analyzer;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileAnalyzer implements Analyzer {
  @Override
  public AnalyzeResult analyze(URI uri) {
    Path path = Path.of(uri);
    AnalyzeResult result = new AnalyzeResult();
    AnalyzeStatus status = AnalyzeStatus.SUCCESS;
    result.newResult("File");
    result.newResult("\t"+path.getFileName());
    try {
      result.newResult("\t" + Files.size(path));
    } catch(Exception e) {
      result.newResult("\tFailed to get size " +e.getMessage() );
      status = AnalyzeStatus.ERROR;
    }
    result.setStatus(status);
    return result;
  }
}
