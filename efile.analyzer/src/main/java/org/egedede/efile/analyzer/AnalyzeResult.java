package org.egedede.efile.analyzer;

public class AnalyzeResult {
  private AnalyzeStatus status = AnalyzeStatus.NOT_ANALYZED;

  private final StringBuilder builder;


  public AnalyzeResult() {
    builder = new StringBuilder();
  }

  public void newResult(String result, Object... args) {
    builder.append('\n').append(String.format(result, args));
  }

  public String getResult() {
    return builder.toString();
  }

  public void setStatus(AnalyzeStatus status) {
    this.status = status;
  }

  public AnalyzeStatus getStatus() {
    return status;
  }
}
