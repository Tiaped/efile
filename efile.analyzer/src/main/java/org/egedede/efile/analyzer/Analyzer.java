package org.egedede.efile.analyzer;

import java.net.URI;

public interface Analyzer {

  AnalyzeResult analyze(URI uri);
}
