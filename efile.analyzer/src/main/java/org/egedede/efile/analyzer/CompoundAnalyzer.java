package org.egedede.efile.analyzer;

import java.net.URI;

public class CompoundAnalyzer implements Analyzer {
  private final Analyzer[] analyzers;


  public CompoundAnalyzer(Analyzer... analyzers) {
    this.analyzers = analyzers;
  }

  @Override
  public AnalyzeResult analyze(URI uri) {
    AnalyzeResult result = new AnalyzeResult();
    for(Analyzer analyzer : analyzers) {
      AnalyzeResult subResult = analyzer.analyze(uri);
      result.newResult(subResult.getResult());
      switch(result.getStatus()) {
        case SUCCESS :
        case NOT_ANALYZED:
          result.setStatus(subResult.getStatus());
      }
    }
    return result;
  }
}
